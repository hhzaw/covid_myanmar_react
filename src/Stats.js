import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';

const Stats = () => {
    const { country } = useParams();
    const [ data, setData ] = useState(
        {
            Country : '',
            TotalCases : 0,
            NewCases : 0,
            TotalDeaths : 0,
            NewDeaths : 0,
            TotalRecovered : 0,
            ActiveCases : 0,
            Serious : 0,
            TotCases_1M : 0,
            TotDeaths_1M : 0
        });

    useEffect(() => {

        const fetchData = async() => {
  
            const response = await axios.get('http://localhost:1200/getOneCountryData/'+country,);
            const c = response.data.data;
            setData(
                {
                    Country : c.Country,
                    TotalCases : c.TotalCases ? c.TotalCases : 0,
                    NewCases : c.NewCases ? c.NewCases : 0,
                    TotalDeaths : c.TotalDeaths ? c.TotalDeaths : 0,
                    NewDeaths : c.NewDeaths ? c.NewDeaths : 0,
                    TotalRecovered : c.TotalRecovered ? c.TotalRecovered : 0,
                    ActiveCases : c.ActiveCases ? c.ActiveCases : 0,
                    Serious : c.Serious ? c.Serious : 0,
                    TotCases_1M : c.TotCases_1M ? c.TotCases_1M : 0,
                    TotDeaths_1M : c.TotDeaths_1M ? c.TotDeaths_1M : 0
                }
            );
            // console.log(result.data.data.Country);

        };
        fetchData();
    },[country])
    //Define when to change
    //So whenever useParams() changes, component will re-render
    //if empty array is given, component will only render at once

    return (
        <React.Fragment>
            <p>{data.Country}</p>
            <p>Total Cases : {data.TotalCases}</p>
            <p>New Cases : {data.NewCases}</p>
            <p>Total Deaths : {data.TotalDeaths}</p>
            <p>Total Recovered : {data.TotalRecovered}</p>
            <p>Active Cases : {data.ActiveCases}</p>
            <p>Serious Cases : {data.Serious}</p>
        </React.Fragment>

    );
}

export default Stats;