import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './Home';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  withRouter
} from 'react-router-dom';
import Stats from './Stats';
import axios from 'axios';
import { findByLabelText } from '@testing-library/dom';


const App = () => {

  const [data, setData] = useState({ countries : [] });
  
  useEffect(() => {
    const fetchData = async() => {
      const response = await axios.get('http://localhost:1200/getCountriesList',);
      const c = response.data;
      setData({
        countries : c
      });
    }
    fetchData();
  }, [])
  return (
    <Router>
      <div style={styles.container}>
        <nav>
          <ul>
            <li>
              <Link to='/'>Home</Link>
            </li>
            <li>
              <Link to='/Stats'>Stats</Link>
            </li>
            {data.countries.map(item => (
              <li key={item.Country}>
                <Link to={`/Stats/${item.Country}`}>{ item.Country }</Link>
              </li>
            ))}
          </ul>
        </nav>

        <Switch>
          <Route exact path='/'>
            <Home/>
          </Route>
          <Route exact path='/Stats/:country' children={<Stats/>}/>
        </Switch>
      </div>
    </Router>
  );
}

const styles = {
  container : {
    display : 'flex',
    flexDirection : 'row'
  }
}
export default App;

